# Anti-Proprietary Action!

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: https://web.archive.org/web/20131130075349/http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://web.archive.org/web/20201213204836if_/https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://web.archive.org/web/20201214072209if_/https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

The GNU head design is from [skwid's GNU Flat Design wallpaper](https://www.gnu.org/graphics/skwid-gnu-flat-design.xcf).

The font is Liberation Mono, which is under the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL).

The vector's reliance on this font means it may not display properly in web browsers.

I recommend you use Inkspace to export it to your desired format. The Inkscpae SVG and "plain" SVG are included here, along with the font for convinence.

I also included a 1000x1000 PNG export of the vector for your sharing on the web. If you want to print the design I recommend you export a higher resolution version.
